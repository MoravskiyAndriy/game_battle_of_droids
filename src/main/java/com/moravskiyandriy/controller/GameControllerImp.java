package com.moravskiyandriy.controller;

import com.moravskiyandriy.gamelogic.GameLogicComponent;
import com.moravskiyandriy.droid.CommonDroidCard;
import com.moravskiyandriy.gamelogic.GameActions;

import java.util.List;

public class GameControllerImp implements GameController {
    private GameActions gameActions;

    public GameControllerImp() {
        gameActions = new GameLogicComponent();
    }

    public final List<CommonDroidCard> getStartCards() {
        return gameActions.getStartCards();
    }

    public final List<CommonDroidCard> getPlayerCards() {
        return gameActions.getPlayerCards();
    }

    public final List<CommonDroidCard> getComputerCards() {
        return gameActions.getComputerCards();
    }

    public final boolean playerChooseCards(final String intArray) {
        return gameActions.playerChooseCards(intArray);
    }

    public final void computerChooseCards() {
        gameActions.computerChooseCards();
    }

    public final boolean checkIfAttackValidAndAttack(final String turn) {
        return gameActions.checkIfAttackValid(turn);
    }

    public final String computerAttack() {
        return gameActions.computerAttack();
    }

    public final boolean checkPlayerCardsAllDead() {
        return gameActions.checkPlayerCardsAllDead();
    }

    public final boolean checkComputerCardsAllDead() {
        return gameActions.checkComputerCardsAllDead();
    }
}
