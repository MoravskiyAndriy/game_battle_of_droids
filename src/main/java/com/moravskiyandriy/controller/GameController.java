package com.moravskiyandriy.controller;

import com.moravskiyandriy.droid.CommonDroidCard;

import java.util.List;

public interface GameController {
    List<CommonDroidCard> getStartCards();

    List<CommonDroidCard> getPlayerCards();

    List<CommonDroidCard> getComputerCards();

    boolean playerChooseCards(final String intArray);

    void computerChooseCards();

    boolean checkIfAttackValidAndAttack(final String turn);

    String computerAttack();

    boolean checkPlayerCardsAllDead();

    boolean checkComputerCardsAllDead();
}
