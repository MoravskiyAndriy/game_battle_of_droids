package com.moravskiyandriy.view;

import com.moravskiyandriy.controller.GameController;
import com.moravskiyandriy.controller.GameControllerImp;
import com.moravskiyandriy.droid.CommonDroidCard;
import com.moravskiyandriy.gamelogic.Constants;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView {
    private static final Logger logger = LogManager.getLogger(ConsoleView.class);
    private static final String WHITE = "\033[0m";
    private GameController gameController;
    private Scanner input = new Scanner(System.in);
    private int GENERAL_CARDS_NUMBER;
    private int GAME_CARDS_NUMBER;

    public ConsoleView() {
        gameController = new GameControllerImp();
        try (InputStream input = ConsoleView.class.getClassLoader().getResourceAsStream("config")) {
            Properties prop = new Properties();
            if(input!=null) {
                prop.load(input);
            }
            GENERAL_CARDS_NUMBER = Optional.ofNullable(prop.
                    getProperty("GENERAL_CARDS_NUMBER")).
                    map(Integer::valueOf).
                    orElse(Constants.GENERAL_CARDS_NUMBER);
            GAME_CARDS_NUMBER = Optional.ofNullable(prop.
                    getProperty("GAME_CARDS_NUMBER")).
                    map(Integer::valueOf).
                    orElse(Constants.GAME_CARDS_NUMBER);
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
    }

    private void showInfo(final String info) {
        System.out.println(info);
    }

    private void showNumbers(final int limit) {
        final int formattingConstant1 = 1;
        final int formattingConstant2 = 5;
        final int formattingConstant3 = 10;
        final String formattingConstant3Spaces = "   ";
        final String formattingConstant4Spaces = "    ";
        for (int i = 1; i < limit + formattingConstant1; i++) {
            if (i == formattingConstant2) {
                System.out.print(WHITE + formattingConstant3Spaces
                        + i + formattingConstant4Spaces);
            } else if (i < formattingConstant3) {
                System.out.print(WHITE + formattingConstant4Spaces
                        + i + formattingConstant4Spaces);
            } else {
                System.out.print(WHITE + formattingConstant3Spaces
                        + i + formattingConstant4Spaces);
            }
        }
        System.out.println();
    }

    private void showCards(final List<CommonDroidCard> cardList) {
        final String formattingConstant1Space = " ";
        System.out.println();
        for (CommonDroidCard cdc : cardList) {
            System.out.print(cdc.getCover()
                    + formattingConstant1Space);
        }
        System.out.println();
        for (CommonDroidCard cdc : cardList) {
            System.out.print(cdc.getHpStringInfo()
                    + formattingConstant1Space);
        }
        System.out.println();
        for (CommonDroidCard cdc : cardList) {
            System.out.print(cdc.getCover()
                    + formattingConstant1Space);
        }
        System.out.println();
        for (CommonDroidCard cdc : cardList) {
            System.out.print(cdc.getAttackStringInfo()
                    + formattingConstant1Space);
        }
        System.out.println();
        for (CommonDroidCard cdc : cardList) {
            System.out.print(cdc.getCover()
                    + formattingConstant1Space);
        }
        System.out.println();
    }

    private void startPhase() {
        logger.info("GAME STARTED.");
        showNumbers(GENERAL_CARDS_NUMBER);
        showCards(gameController.getStartCards());
        System.out.println(WHITE + "\nPlease choose your "
                + GAME_CARDS_NUMBER + " cards: ");
        String playersCards = input.nextLine();
        boolean trying = gameController.playerChooseCards(playersCards);
        while (!trying) {
            System.out.println(WHITE + "\nWrong Input, choose again: ");
            showNumbers(GENERAL_CARDS_NUMBER);
            showCards(gameController.getStartCards());
            System.out.println("\nInput valid " + GAME_CARDS_NUMBER + " cards: ");
            playersCards = input.nextLine();
            trying = gameController.playerChooseCards(playersCards);
        }
        gameController.computerChooseCards();
    }

    private void gamePhase() {
        do {
            showNumbers(GAME_CARDS_NUMBER);
            showCards(gameController.getComputerCards());
            showCards(gameController.getPlayerCards());
            showNumbers(GAME_CARDS_NUMBER);
            if (gameController.checkPlayerCardsAllDead()) {
                break;
            }
            System.out.println(WHITE
                    + "\nPlease, input number of your card and enemy card. ");
            String turn = input.nextLine();
            while (!gameController.checkIfAttackValidAndAttack(turn)) {
                System.out.println(WHITE + "Wrong Input, choose again: ");
                turn = input.nextLine();
            }
            if (!gameController.checkComputerCardsAllDead()) {
                showInfo(gameController.computerAttack());
            } else {
                break;
            }
            System.out.println();
        } while (!gameController.checkPlayerCardsAllDead()
                || !gameController.checkComputerCardsAllDead());
    }

    private void endPhase() {
        System.out.println();
        showNumbers(GAME_CARDS_NUMBER);
        showCards(gameController.getComputerCards());
        showCards(gameController.getPlayerCards());
        showNumbers(GAME_CARDS_NUMBER);
        System.out.println();

        String lastMessage;

        if (gameController.checkPlayerCardsAllDead()) {
            lastMessage = "Computer has won!";
        } else {
            lastMessage = "Player has won!";
        }
        System.out.println(WHITE + lastMessage);
        logger.info("GAME ENDED, " + lastMessage);
    }

    public final void show() {
        startPhase();
        gamePhase();
        endPhase();
    }
}
