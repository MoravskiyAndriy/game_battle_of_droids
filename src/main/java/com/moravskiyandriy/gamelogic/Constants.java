package com.moravskiyandriy.gamelogic;

public class Constants {
    private Constants() {
    }

    public static final String EPIC_DROID_COLOR = "\033[0;96m";
    public static final String COMMON_DROID_COLOR = "\033[0m";
    public static final String RARE_DROID_COLOR = "\033[0;93m";
    public static final String DEAD_DROID_COLOR = "\u001B[91m";
    public static final int GENERAL_CARDS_NUMBER = 12;
    public static final int GAME_CARDS_NUMBER = 6;
    public static final int COATING_LENGTH = 8;
    public static final int MIN_HP = 30;
    public static final int MIN_ATT = 5;
    public static final int MIN_ATT_BASE = 5;
    public static final int MAX_ATT = 10;
    public static final int MAX_ATT_BASE = 10;
    static final int SPECIAL_CARDS_NUMBER = 3;
    public static final double RARE_HP_MODIFIER = 1.4;
    public static final double RARE_ATTACK_MODIFIER = 1.5;
    public static final int RARE_DEFENCE = 3;
    public static final double EPIC_HP_MODIFIER = 4.0;
    public static final double EPIC_ATTACK_MODIFIER = 0.7;
    public static final int EPIC_DEFENCE = 5;
}
