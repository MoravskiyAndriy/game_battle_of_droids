package com.moravskiyandriy.gamelogic;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.moravskiyandriy.droid.CommonDroidCard;
import com.moravskiyandriy.droid.RareDroidCard;
import com.moravskiyandriy.droid.EpicDroidCard;
import com.moravskiyandriy.droid.DeadDroidCard;

/**
 * This is the class which contains main game logic.
 * It forms Droid Card lists and handle different game invents.
 */
public class GameLogicComponent implements GameActions {
    private static final String WHITE = "\033[0m";

    private List<CommonDroidCard> cardList;
    private List<CommonDroidCard> player;
    private List<CommonDroidCard> computer;

    private final int GENERAL_CARDS_NUMBER;
    private final int GAME_CARDS_NUMBER;
    private final int SPECIAL_CARDS_NUMBER;

    private static final Logger logger = LogManager.getLogger(GameLogicComponent.class);

    public GameLogicComponent() {
        Properties prop = new Properties();
        try (InputStream input = GameLogicComponent.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        GENERAL_CARDS_NUMBER = Optional.ofNullable(prop.
                getProperty("GENERAL_CARDS_NUMBER")).
                map(Integer::valueOf).
                orElse(Constants.GENERAL_CARDS_NUMBER);
        GAME_CARDS_NUMBER = Optional.ofNullable(prop.
                getProperty("GAME_CARDS_NUMBER")).
                map(Integer::valueOf).
                orElse(Constants.GAME_CARDS_NUMBER);
        SPECIAL_CARDS_NUMBER = Optional.ofNullable(prop.
                getProperty("SPECIAL_CARDS_NUMBER")).
                map(Integer::valueOf).
                orElse(Constants.SPECIAL_CARDS_NUMBER);
        generateCards();
        player = new LinkedList<>();
        computer = new LinkedList<>();
    }

    /**
     * Generate base list of Droid Cards.
     * Player need to chose a specific number
     * of these card for the game. Computer
     * does it automatically.
     */
    public final void generateCards() {
        cardList = new LinkedList<>();
        for (int i = 0; i < GENERAL_CARDS_NUMBER; i++) {
            cardList.add(new CommonDroidCard());
        }
    }

    /**
     * This method is used to form a list of Player's Droid Cards.
     * It has a variety of verifications,
     * to ensure that the input is correct.
     *
     * @param intArray This is an array of card numbers,
     *                 chosen by the Player.
     */
    public final boolean playerChooseCards(final String intArray) {
        boolean mistakeFlag = true;
        Set<Integer> choiceSet = new HashSet<>();
        String[] integers = intArray.split(" ");
        try {
            for (String st : integers) {
                choiceSet.add(Integer.valueOf(st) - 1);
                if (Integer.valueOf(st) > GENERAL_CARDS_NUMBER
                        || Integer.valueOf(st) <= 0) {
                    mistakeFlag = false;
                    logger.warn("Attempt to input higher, "
                            + "then general number of cards or less "
                            + "or equal zero while selecting cards.");
                }
            }
        } catch (Exception e) {
            logger.warn("Attempt to input incorrect " +
                    "data type while selecting cards.");
            return false;
        }
        if (choiceSet.size() == GAME_CARDS_NUMBER && mistakeFlag) {
            for (int i : choiceSet) {
                player.add(cardList.get(i));
            }
            enhanceCards(player);
            return true;
        }
        logger.warn("Attempt to input wrong quantity "
                + "of numbers while selecting cards.");
        return false;
    }

    /**
     * This method is used to form a list of Computers's Droid Cards.
     */
    public final void computerChooseCards() {
        generateCards();
        HashSet<Integer> choiceSet = new HashSet<>();
        while (choiceSet.size() != GAME_CARDS_NUMBER) {
            Random rand = new Random();
            int number = (int) (rand.nextDouble() * (GENERAL_CARDS_NUMBER));
            choiceSet.add(number);
        }
        for (int i : choiceSet) {
            computer.add(cardList.get(i));
        }
        enhanceCards(computer);
    }

    /**
     * This method is used to enhance some COMMON Droid Cards
     * to RARE or EPIC variations.
     * (cards are chosen for the upgrade randomly)
     *
     * @param droidList This is an array of chosen by player or computer
     *                  COMMON Droid Cards
     */
    private void enhanceCards(final List<CommonDroidCard> droidList) {
        Random rand = new Random();
        Set<Integer> specialCardSet = new HashSet<>();
        while (specialCardSet.size() < SPECIAL_CARDS_NUMBER) {
            int choice = (int) (rand.nextDouble() * GAME_CARDS_NUMBER);
            specialCardSet.add(choice);
        }
        List<Integer> fromSetList = new ArrayList<>(specialCardSet);
        for (int i = 0; i < SPECIAL_CARDS_NUMBER - 1; i++) {
            droidList.set(fromSetList.get(i),
                    new RareDroidCard(droidList.get(fromSetList.get(i))));
        }
        droidList.set(fromSetList.get(SPECIAL_CARDS_NUMBER - 1),
                new EpicDroidCard(droidList
                        .get(fromSetList.get(SPECIAL_CARDS_NUMBER - 1))));
    }

    private void setDeadDroidCards() {
        for (int i = 0; i < GAME_CARDS_NUMBER; i++) {
            if (!player.get(i).isAlive()) {
                player.set(i, new DeadDroidCard());
            }
            if (!computer.get(i).isAlive()) {
                computer.set(i, new DeadDroidCard());
            }
        }
    }

    /**
     * This method is used to get the list of
     * Starting Droid Cards.
     *
     * @return list of Starting Droid Cards.
     */
    public final List<CommonDroidCard> getStartCards() {
        return cardList;
    }

    /**
     * This method is used to get the list of
     * Player's Droid Cards during the game.
     *
     * @return list of Player's Droid Cards.
     */
    public final List<CommonDroidCard> getPlayerCards() {
        setDeadDroidCards();
        return player;
    }

    /**
     * This method is used to get the list of
     * Computers's Droid Cards during the game.
     *
     * @return list of Computer's Droid Cards.
     */
    public final List<CommonDroidCard> getComputerCards() {
        setDeadDroidCards();
        return computer;
    }

    /**
     * This method is used when it is turn of Player
     * to attack.
     *
     * @param playerCard   This is a number of Player's card
     *                     which will be used to attack.
     * @param computerCard This is a number of Computer's card
     *                     which will be attacked.
     * @return false if there is attempt to use or attack
     * a 'dead' Droid Card.
     */
    public final boolean playerAttack(final int playerCard,
                                      final int computerCard) {
        if (player.get(playerCard).isAlive()
                && computer.get(computerCard).isAlive()) {
            player.get(playerCard).dealDamage(computer.get(computerCard));
            return true;
        }
        logger.warn("Attempt to select already 'dead' cards "
                + "while selecting cards for attack.");
        return false;
    }

    /**
     * This method make different verifications
     * to ensure that Player's attack is valid.
     *
     * @return false if there is attempt to use or attack
     * Droid Card with incorrect number.
     */
    public final boolean checkIfAttackValid(final String turn) {
        try {
            if (Integer.valueOf(turn.split(" ")[0]) > GAME_CARDS_NUMBER
                    || (Integer.valueOf(turn.split(" ")[0]) <= 0)
                    || Integer.valueOf(turn.split(" ")[1]) > GAME_CARDS_NUMBER
                    || (Integer.valueOf(turn.split(" ")[1]) <= 0)) {
                logger.warn("Attempt to input card numbers out "
                        + "of range while selecting cards for attack.");
                return false;
            }
        } catch (Exception e) {
            logger.warn("Attempt to input wrong data type "
                    + "while selecting cards for attack.");
            return false;
        }
        return playerAttack(Integer.valueOf(turn.split(" ")[0]) - 1,
                Integer.valueOf(turn.split(" ")[1]) - 1);
    }

    /**
     * This method is used when it is turn of computer
     * to attack.
     *
     * @return String representation of Computer's actions.
     */
    public final String computerAttack() {
        Random rand = new Random();
        int computerCard;
        int playerCard;
        do {
            computerCard = (int) (rand.nextDouble()
                    * GAME_CARDS_NUMBER);
            playerCard = (int) (rand.nextDouble()
                    * GAME_CARDS_NUMBER);
        } while (!player.get(playerCard).isAlive()
                || !computer.get(computerCard).isAlive());
        String computerAttackInfo = WHITE + "Computer turn:"
                + (computerCard + 1) + "->"
                + (playerCard + 1);
        computer.get(computerCard).dealDamage(player.get(playerCard));
        return computerAttackInfo;
    }

    /**
     * This method checks if all player's Droid Cards are dead.
     *
     * @return true if all Player's Droid Cards are 'dead'.
     */
    public final boolean checkPlayerCardsAllDead() {
        boolean flag = true;
        for (CommonDroidCard cdc : player) {
            if (cdc.isAlive()) {
                flag = false;
            }
        }
        return flag;
    }

    /**
     * This method checks if all computer's Droid Cards are dead.
     *
     * @return true if all Computer's Droid Cards are 'dead'.
     */
    public final boolean checkComputerCardsAllDead() {
        boolean flag = true;
        for (CommonDroidCard cdc : computer) {
            if (cdc.isAlive()) {
                flag = false;
            }
        }
        return flag;
    }
}
