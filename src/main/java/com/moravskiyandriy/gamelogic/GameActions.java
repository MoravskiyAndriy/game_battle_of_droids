package com.moravskiyandriy.gamelogic;

import com.moravskiyandriy.droid.CommonDroidCard;

import java.util.List;

public interface GameActions {
    void generateCards();

    boolean playerChooseCards(final String intArray);

    void computerChooseCards();

    boolean playerAttack(final int playerCard, final int computerCard);

    String computerAttack();

    boolean checkPlayerCardsAllDead();

    boolean checkComputerCardsAllDead();

    boolean checkIfAttackValid(final String turn);

    List<CommonDroidCard> getStartCards();

    List<CommonDroidCard> getPlayerCards();

    List<CommonDroidCard> getComputerCards();
}