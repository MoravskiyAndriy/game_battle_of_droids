package com.moravskiyandriy.droid;

import com.moravskiyandriy.gamelogic.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;

import static com.moravskiyandriy.gamelogic.Constants.EPIC_DROID_COLOR;

/**
 * This class represents Droid Card of EPIC quality,
 * which has it's hp greatly increased and attack moderately decreased,
 * compare to COMMON droid cards. This Droid Card has
 * better, then a card of RARE quality and a special power:
 * though it has comparatively low damage, EPIC Droid Card deals
 * double damage to cards of COMMON quality.
 */
public class EpicDroidCard extends CommonDroidCard {
    private static final Logger logger = LogManager.getLogger(EpicDroidCard.class);
    private final int DEFENCE;
    private final int COATING_LENGTH;

    public EpicDroidCard(final CommonDroidCard commonDroid) {
        double hpModifier;
        double attackModifier;

        Properties prop = new Properties();
        try (InputStream input = EpicDroidCard.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        COATING_LENGTH = Optional.ofNullable(prop.
                getProperty("COATING_LENGTH")).
                map(Integer::valueOf).
                orElse(Constants.COATING_LENGTH);
        DEFENCE = Optional.ofNullable(prop.
                getProperty("EPIC_DEFENCE")).
                map(Integer::valueOf).
                orElse(Constants.EPIC_DEFENCE);
        hpModifier = Optional.ofNullable(prop.
                getProperty("EPIC_HP_MODIFIER")).
                map(Double::valueOf).
                orElse(Constants.EPIC_HP_MODIFIER);
        attackModifier = Optional.ofNullable(prop.
                getProperty("EPIC_ATTACK_MODIFIER")).
                map(Double::valueOf).
                orElse(Constants.EPIC_ATTACK_MODIFIER);

        setCover(EPIC_DROID_COLOR + "@@@@@@@@");
        setHpStringInfo(EPIC_DROID_COLOR);
        setAttackStringInfo(EPIC_DROID_COLOR);
        this.setHp((int) ((double) commonDroid.getHp() * hpModifier));
        this.setMinAttack((int)
                ((double) commonDroid.getMinAttack() * attackModifier));
        if (this.getMinAttack() < Constants.EPIC_DEFENCE) {
            this.setMinAttack(Constants.EPIC_DEFENCE);
        }
        this.setMaxAttack((int)
                ((double) commonDroid.getMaxAttack() * attackModifier));
        setRarity(Rarity.EPIC);
        setColour(EPIC_DROID_COLOR);
        setAlive(true);
        StringBuilder forHp = new StringBuilder("@hp=" + getHp() + "@");
        while (forHp.length() < COATING_LENGTH) {
            forHp.append("@");
        }
        StringBuilder forAttack = new StringBuilder("@" + getMinAttack() + "-"
                + getMaxAttack() + "@");
        while (forAttack.length() < COATING_LENGTH) {
            forAttack.append("@");
        }
        setHpStringInfo(getHpStringInfo() + forHp);
        setAttackStringInfo(getAttackStringInfo() + forAttack);
    }

    protected final void takeDamage(final int damage) {
        setHp(this.getHp() - damage + DEFENCE);
        StringBuilder forHp = new StringBuilder("@hp=" + getHp() + "@");
        while (forHp.length() < COATING_LENGTH) {
            forHp.append("@");
        }
        setHpStringInfo(EPIC_DROID_COLOR + forHp);
        if (this.getHp() <= 0) {
            this.setAlive(false);
        }
    }

    public final void dealDamage(final CommonDroidCard commonDroidCard) {
        Random rand = new Random();
        int damage = (int) (rand.nextDouble() * (getMaxAttack()
                - getMinAttack()) + getMinAttack());
        if (commonDroidCard.getRarity() == Rarity.COMMON) {
            damage = damage * 2;
        }
        commonDroidCard.takeDamage(damage);
    }
}
