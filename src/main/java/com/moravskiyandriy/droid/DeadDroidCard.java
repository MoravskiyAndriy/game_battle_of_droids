package com.moravskiyandriy.droid;

import static com.moravskiyandriy.gamelogic.Constants.DEAD_DROID_COLOR;

public class DeadDroidCard extends CommonDroidCard {
    private static final String BACKGROUND = "########";

    public DeadDroidCard() {
        setCover(DEAD_DROID_COLOR + BACKGROUND);
        setHpStringInfo(DEAD_DROID_COLOR + BACKGROUND);
        setAttackStringInfo(DEAD_DROID_COLOR + BACKGROUND);
        setAlive(false);
    }
}
