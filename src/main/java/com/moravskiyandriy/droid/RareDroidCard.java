package com.moravskiyandriy.droid;

import com.moravskiyandriy.droid.CommonDroidCard;
import com.moravskiyandriy.gamelogic.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

import static com.moravskiyandriy.gamelogic.Constants.RARE_DROID_COLOR;

/**
 * This class represents Droid Card of RARE quality,
 * which has it's hp and attack moderately increased,
 * compare to COMMON droid cards. This Droid Card has
 * defense value, which lower any damage dealt to this card.
 */
public class RareDroidCard extends CommonDroidCard {
    private static final Logger logger = LogManager.getLogger(RareDroidCard.class);
    private final int DEFENCE;
    private final int COATING_LENGTH;

    public RareDroidCard(final CommonDroidCard commonDroid) {
        double hpModifier;
        double attackModifier;

        Properties prop = new Properties();
        try (InputStream input = com.moravskiyandriy.droid.RareDroidCard.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        COATING_LENGTH = Optional.ofNullable(prop.
                getProperty("COATING_LENGTH")).
                map(Integer::valueOf).
                orElse(Constants.COATING_LENGTH);
        DEFENCE = Optional.ofNullable(prop.
                getProperty("RARE_DEFENCE")).
                map(Integer::valueOf).
                orElse(Constants.RARE_DEFENCE);
        hpModifier = Optional.ofNullable(prop.
                getProperty("RARE_HP_MODIFIER")).
                map(Double::valueOf).
                orElse(Constants.RARE_HP_MODIFIER);
        attackModifier = Optional.ofNullable(prop.
                getProperty("RARE_ATTACK_MODIFIER")).
                map(Double::valueOf).
                orElse(Constants.RARE_ATTACK_MODIFIER);

        setCover(RARE_DROID_COLOR + "@@@@@@@@");
        setHpStringInfo(RARE_DROID_COLOR);
        setAttackStringInfo(RARE_DROID_COLOR);
        this.setHp((int) ((double) commonDroid.getHp() * hpModifier));
        this.setMinAttack((int)
                ((double) commonDroid.getMinAttack() * attackModifier));
        this.setMaxAttack((int)
                ((double) commonDroid.getMaxAttack() * attackModifier));
        setRarity(Rarity.RARE);
        setColour(RARE_DROID_COLOR);
        setAlive(true);
        StringBuilder forHp = new StringBuilder("@hp=" + getHp() + "@");
        while (forHp.length() < COATING_LENGTH) {
            forHp.append("@");
        }
        StringBuilder forAttack = new StringBuilder("@" + getMinAttack() + "-"
                + getMaxAttack() + "@");
        while (forAttack.length() < COATING_LENGTH) {
            forAttack.append("@");
        }
        setHpStringInfo(getHpStringInfo() + forHp);
        setAttackStringInfo(getAttackStringInfo() + forAttack);
    }

    protected final void takeDamage(final int damage) {
        setHp(this.getHp() - damage + DEFENCE);
        StringBuilder forHp = new StringBuilder("@hp=" + getHp() + "@");
        while (forHp.length() < COATING_LENGTH) {
            forHp.append("@");
        }
        setHpStringInfo(RARE_DROID_COLOR + forHp);
        if (this.getHp() <= 0) {
            this.setAlive(false);
        }
    }
}
