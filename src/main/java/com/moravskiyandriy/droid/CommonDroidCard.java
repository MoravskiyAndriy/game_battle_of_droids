package com.moravskiyandriy.droid;

import com.moravskiyandriy.gamelogic.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;

import static com.moravskiyandriy.gamelogic.Constants.COMMON_DROID_COLOR;

/**
 * This class represents base Droid Card of COMMON quality,
 * which has just hp and attack parameters and no other
 * special features. It can, however, be upgraded to
 * stronger versions.
 */
public class CommonDroidCard {
    private static final Logger logger = LogManager.getLogger(CommonDroidCard.class);
    private static final int ZERO_COMPENSATOR = 1;
    private String colour;
    private String cover;
    private String hpStringInfo;
    private String attackStringInfo;
    private boolean isAlive;

    private int hp;
    private int minAttack;
    private int maxAttack;
    private Rarity rarity;
    private int COATING_LENGTH;

    public CommonDroidCard() {
        Properties prop = new Properties();
        try (InputStream input = CommonDroidCard.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        int minHp;
        int minAtt;
        int minAttBase;
        int maxAtt;
        int maxAttBase;

        minHp = Optional.ofNullable(prop.
                getProperty("MIN_HP")).
                map(Integer::valueOf).
                orElse(Constants.MIN_HP);
        minAtt = Optional.ofNullable(prop.
                getProperty("MIN_ATT")).
                map(Integer::valueOf).
                orElse(Constants.MIN_ATT);
        minAttBase = Optional.ofNullable(prop.
                getProperty("MIN_ATT_BASE")).
                map(Integer::valueOf).
                orElse(Constants.MIN_ATT_BASE);
        maxAtt = Optional.ofNullable(prop.
                getProperty("MAX_ATT")).
                map(Integer::valueOf).
                orElse(Constants.MAX_ATT);
        maxAttBase = Optional.ofNullable(prop.
                getProperty("MAX_ATT_BASE")).
                map(Integer::valueOf).
                orElse(Constants.MAX_ATT_BASE);
        COATING_LENGTH = Optional.ofNullable(prop.
                getProperty("COATING_LENGTH")).
                map(Integer::valueOf).
                orElse(Constants.COATING_LENGTH);

        Random rand = new Random();
        hp = (int) (rand.nextDouble() * (minHp
                + ZERO_COMPENSATOR) + minHp);
        minAttack = (int) (rand.nextDouble() * (minAtt
                + ZERO_COMPENSATOR) + minAttBase);
        maxAttack = (int) (rand.nextDouble() * (maxAtt
                + ZERO_COMPENSATOR) + maxAttBase);
        rarity = Rarity.COMMON;
        setColour(COMMON_DROID_COLOR);
        setAlive(true);
        setCover(COMMON_DROID_COLOR + "@@@@@@@@");
        setHpStringInfo(COMMON_DROID_COLOR);
        setAttackStringInfo(COMMON_DROID_COLOR);
        StringBuilder forHp = new StringBuilder("@hp=" + getHp() + "@");
        while (forHp.length() < COATING_LENGTH) {
            forHp.append("@");
        }
        StringBuilder forAttack = new StringBuilder("@" + getMinAttack() + "-"
                + getMaxAttack() + "@");
        while (forAttack.length() < COATING_LENGTH) {
            forAttack.append("@");
        }
        hpStringInfo += forHp;
        attackStringInfo += forAttack;
    }

    public final String getCover() {
        return cover;
    }

    final void setCover(final String cover) {
        this.cover = cover;
    }

    public final String getHpStringInfo() {
        return hpStringInfo;
    }

    final void setHpStringInfo(final String hpStringInfo) {
        this.hpStringInfo = hpStringInfo;
    }

    public final String getAttackStringInfo() {
        return attackStringInfo;
    }

    final void setAttackStringInfo(final String attackStringInfo) {
        this.attackStringInfo = attackStringInfo;
    }

    public final boolean isAlive() {
        return isAlive;
    }

    final void setAlive(final boolean alive) {
        isAlive = alive;
    }

    final Rarity getRarity() {
        return rarity;
    }

    final int getHp() {
        return hp;
    }

    final void setHp(final int hp) {
        this.hp = hp;
    }

    final int getMinAttack() {
        return minAttack;
    }

    final void setMinAttack(final int minAttack) {
        this.minAttack = minAttack;
    }

    final int getMaxAttack() {
        return maxAttack;
    }

    final void setMaxAttack(final int maxAttack) {
        this.maxAttack = maxAttack;
    }

    private String getColour() {
        return colour;
    }

    final void setColour(String colour) {
        this.colour = colour;
    }

    final void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommonDroidCard that = (CommonDroidCard) o;
        return isAlive() == that.isAlive()
                && getHp() == that.getHp()
                && getMinAttack() == that.getMinAttack()
                && getMaxAttack() == that.getMaxAttack()
                && getHpStringInfo().equals(that.getHpStringInfo())
                && getAttackStringInfo().equals(that.getAttackStringInfo())
                && getRarity() == that.getRarity();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHpStringInfo(), getAttackStringInfo(),
                isAlive(), getHp(), getMinAttack(), getMaxAttack(), getRarity());
    }

    @Override
    public String toString() {
        return "CommonDroidCard{" +
                "colour='" + colour + '\'' +
                ", cover='" + cover + '\'' +
                ", hpStringInfo='" + hpStringInfo + '\'' +
                ", attackStringInfo='" + attackStringInfo + '\'' +
                ", isAlive=" + isAlive +
                ", hp=" + hp +
                ", minAttack=" + minAttack +
                ", maxAttack=" + maxAttack +
                ", rarity=" + rarity +
                '}';
    }

    protected void takeDamage(final int damage) {
        setHp(this.hp - damage);
        StringBuilder forHp = new StringBuilder("@hp=" + getHp() + "@");
        while (forHp.length() < COATING_LENGTH) {
            forHp.append("@");
        }
        hpStringInfo = getColour() + forHp;
        if (this.getHp() <= 0) {
            this.setAlive(false);
        }
    }

    public void dealDamage(final CommonDroidCard commonDroidCard) {
        Random rand = new Random();
        int damage = (int) (rand.nextDouble() * (getMaxAttack()
                - getMinAttack()) + getMinAttack());
        commonDroidCard.takeDamage(damage);
    }
}

