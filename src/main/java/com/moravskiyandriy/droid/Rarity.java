package com.moravskiyandriy.droid;

enum Rarity {
    COMMON, RARE, EPIC
}
