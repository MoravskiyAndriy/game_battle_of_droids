package com.moravskiyandriy;

import com.moravskiyandriy.view.ConsoleView;

public final class Application {
    private Application() {
    }

    public static void main(final String[] args) {
        new ConsoleView().show();
    }
}
